![logo_baja.png](https://bitbucket.org/repo/66a45R/images/3377336133-logo_baja.png)

# README #

## EVA es el nombre de la aplicación multi-plataforma desarrollada por Nerdy Trust con el propósito de concientizar a las personas acerca del valor de sus datos personales.
Los datos personales son tratados con un valor económico lo que hace posible que las personas tengan un punto de comparación respecto a la cantidada de información que proporcionan a grupos, empresas o instituciones y su valor monetario.

Nerdy Trust desarrolló EVA (personal data Economic VAlue) para el reto “Valora tus datos personales” lanzado por el INAI (Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales) en la plataforma de Codeando México.

La aplicación EVA está dividida en secciones para facilitar la navegación e involucrar al usuario para que pueda utilizar la aplicación de manera simple.

##### **Inicio**

La primera vez que se entra a la aplicación, se muestra un slider que contiene la información básica sobre la valoración de los datos personales, este contenido se puede compartir en las redes sociales o por otros medios como el correo electrónico o mensajería instantánea, las siguientes veces que se use la aplicación, se llevará al usuario directamente a la sección “Valor de tus datos”.

##### **Valor de tus datos**

Muestra el valor económico de tus datos personales segmentado por grupos, en esta pantalla el usuario puede crear un nuevo grupo, eliminar alguno de los existentes y dirigirse a la pantalla “Nivel de riesgo”.

##### **Nivel de riesgo**

Esta vista nos permite interactuar con la información proporciona a la aplicación, donde podremos ver de manera gráfica el nivel de riesgo que implica cada grupo de datos y cómo se compone.

##### **Valor de tus datos en redes sociales**

Esta vista nos permite seleccionar las redes sociales en las que interactuamos de manera frecuente, pues el uso constante provee datos importantes para ciertos grupos y empresas quienes ven un gran valor económico al conocer las tendencias de los usuarios, por ello hemos considerado el valor aproximado para cada una de ellas.

##### **Valor de tus datos de un grupo**

Esta vista nos permitirá seleccionar la información que proporcionamos a un grupo para posteriormente conocer su valor económico, los campos son de selección y divididos por su nivel de riesgo.

##### **Preguntas frecuentes**

Muestra aquellas preguntas frecuentes acerca de los datos personales, permite compartir esta información en redes sociales o por otros medios como el correo electrónico.

##### **Glosario**

Muestra aquellos conceptos acerca de los datos personales, permite compartir esta información en redes sociales o por otros medios como el correo electrónico.

##### **Acerca de**

Muestra información acerca del desarrollador.

### **¿ Cómo instalar la aplicación** ? ###

* La aplicación puede ser instalada desde sus links oficiales

    * Apple Store (próximamente
    * Google Play (próximamente)

La última versión para android también puede ser instalada desde [Aquí](https://ec8be031fc8e71d1abcd-53c78656c1cdb03eb0c218566e8554db.ssl.cf5.rackcdn.com/EVA_1.0.0.apk)

Y la version de IOS desde [Aquí](https://ec8be031fc8e71d1abcd-53c78656c1cdb03eb0c218566e8554db.ssl.cf5.rackcdn.com/index.html)

### **Construir la aplicación desde el fuente** ###

Primero necesitas los siguientes paquetes instalados en tu computadora.

* Git
* Node.js (with NPM)
* Bower
* Ember CLI
* Ionic 
* Apache Cordova

> Adicionalmente deberas tener en correcto funcionamiento un entorno de desarrollo de aplicaciones moviles, ya sea para Android o para IOS.

##### **Instalación**

git clone <repository-url> 

cambiar al directorio creado __cd eva__

npm install

bower install

##### **Ejecutar en el navegador**

Desde el command-line podemos invocar el servidor de Ember, lo que nos permite visualizar la aplicación directamente en el navegador, con ello podemos validar su funcionamiento, las limitantes son las funciones nativas del dispositivo como compartir.

+ en el command-line: ember server
    + Acceder en nuestro navegador a: http://localhost:4200

![emberserver.png](https://bitbucket.org/repo/66a45R/images/3710828856-emberserver.png)

![emberweb.png](https://bitbucket.org/repo/66a45R/images/2308747435-emberweb.png)


##### **Ejecutar en emulador**
+ cordova run <platform>

##### **Ejecutar en el dispositivo**
+ cordova run <platform> --device


### ** Wiki **

Puedes encontrar más información del proyecto en: [Wiki](https://bitbucket.org/nerdyteam2/eva/wiki/Home)

Así como consultar el manual de usuario disponible e: [Manual](https://bitbucket.org/nerdyteam2/eva/wiki/Home)