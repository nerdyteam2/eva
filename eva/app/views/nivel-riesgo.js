import Ember from 'ember';
export default Ember.View.extend({
  didInsertElement : function(){

    var sideslider = $('[data-toggle=collapse-side]');
            var sel = sideslider.attr('data-target');
            var sel2 = sideslider.attr('data-target-2');
            sideslider.click(function(event){
                $(sel).toggleClass('in');
                $(sel2).toggleClass('out');
            });

  	window.appdata = JSON.parse(localStorage.getItem('eva-appdata'));
  	var pdv = 0.0;
  	var pnv = 0.0;
  	var tpd = 0.0;

  	pnv = window.appdata.pnv;
  	pdv = window.appdata.pdv;
  	tpd = pnv + pdv;

  		for(var i=0;i<window.appdata.pd.length;i++){
	  		//var prc = 100*window.appdata.pd[i].value/pdv;
        var prc = 100;
        if( window.appdata.pd[i].value < 5000){
          prc = 100*window.appdata.pd[i].value/5000;
        }

	  		$('#grupos-datos').append(
	  		'<div class="col-xs-12 grafica2 relative">'+
        '<span class="btn btn-question4"></span>'+
  			'<h1 class="app text-left margin-top">Valor de los datos que ingresaste al Grupo : '+ window.appdata.pd[i].name +'</h1>'+
  			    '<div class="range col-xs-12 no-padding">'+
  			    '<input type="range" name="range" min="1" max="100" value="'+prc+'" readonly>'+
  			    '</div>'+
  			'</div>'
	  		);
  		}

      $('.grafica2').click(function(){
        $('#duda3 .modal-header h1, #duda3 .modal-body b').text('');
        var val = $(this).find('input[type="range"]').val();
        if(val < 33){
           $('#duda3 .modal-header h1.green').text('Nivel Estandar');
          $('#duda3 .modal-body b.green').text('Bien!, aunque el nivel de riesgo para este grupo es bajo, asegúrate de ofrecer siempre solo los datos necesarios a instituciones o negocios que tengan una política transparente de tratamiento de datos personales.');
        }
        else if(val < 66){
           $('#duda3 .modal-header h1.orange').text('Nivel Sensible');
          $('#duda3 .modal-body b.orange').text('Precaución!, estas en un nivel medio pero deberías ser más cuidadoso con la cantidad y tipo de datos que ofreces. No te confíes!, verifica como son utilizados los datos que haz ofrecido.');
        }
        else{
          $('#duda3 .modal-header h1.especial').text('Nivel Especial');
          $('#duda3 .modal-body b.especial').text('Cuidado!, verifica que datos haz entregado, acércate con la institución o negocio y asegúrate de que tus datos no sean utilizados mal o con fines de lucro.');
        }

        $('#duda3').modal();
      });


      //Graficas
      var chart;
      var legend;
      var selected;

      window.types = [
      {
          type: "",
          percent: 25, 
          color: "#009245",
          subs: [ ]
      },
      {
          type: "",
          percent: 25,
          color: "#FF931E",
          subs: [ ]
      },
      {
          type: "",
          percent: 25,
          color: "#ED1C24",
          subs: [ ]
      },
      {
          type: "",
          percent: 25,
          color: "#0000FF",
          subs: [ ]
      }
      ];

      var totalLow    = 0;
      var totalMed    = 0;
      var totalHig    = 0;
      var totalAll    = 0;
      
      var percLow = 0.0;
      var percMed = 0.0;
      var percHig = 0.0;
      var percSoc = 0.0;

      for(var i=0;i<window.appdata.pd.length;i++){
        totalAll   += window.appdata.pd[i].llv + window.appdata.pd[i].mlv + window.appdata.pd[i].hlv;
        totalLow += window.appdata.pd[i].llv;
        totalMed += window.appdata.pd[i].mlv;
        totalHig += window.appdata.pd[i].hlv;
      }
      totalAll += window.appdata.pnv;
      
      percLow = totalLow/totalAll*100;
      percMed = totalMed/totalAll*100;
      percHig = totalHig/totalAll*100;
      percSoc = window.appdata.pnv/totalAll*100;

      types[0].percent = percLow;
      types[1].percent = percMed;
      types[2].percent = percHig;

      $('.datos:nth-of-type(2) span').text(percLow.toFixed(2)+' %');
      $('.datos:nth-of-type(3) span').text(percMed.toFixed(2)+' %');
      $('.datos:nth-of-type(4) span').text(percHig.toFixed(2)+' %');
      $('.datos:nth-of-type(5) span').text(percSoc.toFixed(2)+' %');

      for(var i=0;i<window.appdata.pd.length;i++){
        window.appdata.pd[i].llp = window.appdata.pd[i].llv/totalAll*100;
        window.appdata.pd[i].mlp = window.appdata.pd[i].mlv/totalAll*100;
        window.appdata.pd[i].hlp = window.appdata.pd[i].hlv/totalAll*100;

        types[0].subs.push({
          type: window.appdata.pd[i].name, 
          percent: window.appdata.pd[i].llp 
        });
        types[1].subs.push({
          type: window.appdata.pd[i].name, 
          percent: window.appdata.pd[i].mlp 
        });
        types[2].subs.push({
          type: window.appdata.pd[i].name, 
          percent: window.appdata.pd[i].hlp 
        });
      }

        types[3].percent = percSoc;
        types[3].subs.push({
          type: "RS", 
          percent: percSoc 
        });

      function generateChartData () {
          var chartData = [];
          for (var i = 0; i < types.length; i++) {
              if (i == selected) {
                  for (var x = 0; x < types[i].subs.length; x++) {
                      chartData.push({
                          type: types[i].subs[x].type,
                          percent: types[i].subs[x].percent,
                          color: types[i].color,
                          pulled: true
                      });
                  }
              }
              else {
                  chartData.push({
                      type: types[i].type,
                      percent: types[i].percent,
                      color: types[i].color,
                      id: i
                  });
              }
          }
          return chartData;
      }

      //AmCharts.ready(function() {
          // PIE CHART
          chart = new AmCharts.AmPieChart();
          chart.dataProvider = generateChartData();
          chart.titleField = "type";
          chart.valueField = "percent";
          chart.outlineColor = "#FFFFFF";
          chart.outlineAlpha = 0.8;
          chart.outlineThickness = 2;
          chart.colorField = "color";
          chart.pulledField = "pulled";
        
          chart.path = "";
          
          // ADD TITLE
          chart.addTitle("Click en una sección para ver más detalles");
          
          // AN EVENT TO HANDLE SLICE CLICKS
          chart.addListener("clickSlice", function (event) {
              if (event.dataItem.dataContext.id != undefined) {
                  selected = event.dataItem.dataContext.id;
              }
              else {
                  selected = undefined;
              }
              chart.dataProvider = generateChartData();
              chart.validateData();
              $('#chartdiv a').text('');
          });

          // WRITE
          chart.write("chartdiv");
          $('#chartdiv a').text('');
     // });

  },
  willDestroyElement: function()
  {
    this._super();
  }
});