import Ember from 'ember';
export default Ember.View.extend({
  didInsertElement : function(){
  	var sideslider = $('[data-toggle=collapse-side]');
    var sel = sideslider.attr('data-target');
    var sel2 = sideslider.attr('data-target-2');
    sideslider.click(function(event){
        $(sel).toggleClass('in');
        $(sel2).toggleClass('out');
    });


    setTimeout(function(){

      if(sessionStorage.getItem('eva-appdata-2')=='1'){
        $("#myModal3").hide();
      }

      $("#crea-grupo").click(function() {
          $("#myModal3").animate({
              height: "toggle",
              opacity: "toggle"
          }, "slow");
          sessionStorage.setItem('eva-appdata-2','1');
        });
        
    },100)

    

    $('button').last().click(function(){
       window.appdata = JSON.parse(localStorage.getItem('eva-appdata'));
       var group = {};
       group.name= $('#nombre-grupo').val();
       group.llv=0.0;
       group.mlv=0.0;
       group.hlv=0.0;
       group.llc=0.0;
       group.mlc=0.0;
       group.hlc=0.0;
       group.tru=2.5;
       group.value = 0.0;


       if(group.name.length < 3 ){
         $('.modal-title').text('Error de validación');
         $('.modal-body').text('Porfavor ingresa un nombre para el grupo de almenos 3 digitos de longitud');
         $('#alert').modal();
         return;
       }


       $('.col-xs-6:nth-of-type(2) input').each(function(){
          if($(this).is(':checked')){
            group.llc+=1;
          }
       });
       $('.col-xs-6:nth-of-type(3) input').each(function(){
          if($(this).is(':checked')){
            group.llc+=1;
          }
       });


       $('.col-xs-6:nth-of-type(4) input').each(function(){
          if($(this).is(':checked')){
            group.mlc+=1;
          }
       });
       $('.col-xs-6:nth-of-type(5) input').each(function(){
          if($(this).is(':checked')){
            group.mlc+=1;
          }
       });


       $('.col-xs-12 input[type="checkbox"]').each(function(){
          if($(this).is(':checked')){
            group.hlc+=1;
          }
       });


      if(group.llc == 0 && group.mlc==0 && group.hlc==0){
         $('.modal-title').text('Error de validación');
         $('.modal-body').text('Selecciona almenos una casilla.');
         $('#alert').modal();
         return;
      }

      var auxl = 0;
      for(var i = 1; i < (group.llc+1) ; i++){
        auxl+=i;
      }
      group.lla = auxl;

      auxl = 0;
      for(var i = 2; i < (group.mlc+2) ; i++){
        auxl+=i;
      }
      group.mla = auxl;

      auxl = 0;
      for(var i = 3; i < (group.hlc+3) ; i++){
        auxl+=i;
      }
      group.hla = auxl;

      group.tru = parseInt($("#range").val());

      group.llv = ( (group.lla) / (group.llc+group.mlc+group.hlc) ) * group.tru * 150;
      group.mlv = ( (group.mla) / (group.llc+group.mlc+group.hlc) ) * group.tru * 150;
      group.hlv = ( (group.hla) / (group.llc+group.mlc+group.hlc) ) * group.tru * 150;

      group.value  = group.llv+group.mlv+group.hlv;

      window.appdata.ft = false;
      window.appdata.pd.push(group);

      window.appdata.pdv = 0.0;
      for(var i =0 ; i< window.appdata.pd.length; i++){
        window.appdata.pdv += window.appdata.pd[i].value;
      }
      localStorage.setItem('eva-appdata',JSON.stringify(window.appdata));


      document.location.href='#/valor-grupo';

    });

  },
  willDestroyElement: function()
  {
    this._super();

  }
});