import Ember from 'ember';
export default Ember.View.extend({
  didInsertElement : function(){
  	var sideslider = $('[data-toggle=collapse-side]');
            var sel = sideslider.attr('data-target');
            var sel2 = sideslider.attr('data-target-2');
            sideslider.click(function(event){
                $(sel).toggleClass('in');
                $(sel2).toggleClass('out');
            });
    
    setTimeout(function(){

      window.appdata = JSON.parse(localStorage.getItem('eva-appdata'));
      if(appdata.pd.length > 0)
      {
        document.location.href="#/valor-grupo";
      }
      else
      {

        var pnv = window.appdata.pnv?window.appdata.pnv:0;
        var pdv = window.appdata.pdv?window.appdata.pdv:0;
        var tpd = pnv + pdv;

        window.appdata.tpd = tpd;
        
        $('#valor-datos').text(window.appdata.tpd.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
      }
     
    },100);

  },
  willDestroyElement: function()
  {
    this._super();

  }
});