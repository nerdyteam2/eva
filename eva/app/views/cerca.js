import Ember from 'ember';
export default Ember.View.extend({
  didInsertElement : function(){
  	var sideslider = $('[data-toggle=collapse-side]');
    var sel = sideslider.attr('data-target');
    var sel2 = sideslider.attr('data-target-2');
    sideslider.click(function(event){
        $(sel).toggleClass('in');
        $(sel2).toggleClass('out');
    });
  },
  willDestroyElement: function()
  {
    this._super();

  }
});