import Ember from 'ember';
export default Ember.View.extend({
  didInsertElement : function(){

    var sideslider = $('[data-toggle=collapse-side]');
            var sel = sideslider.attr('data-target');
            var sel2 = sideslider.attr('data-target-2');
            sideslider.click(function(event){
                $(sel).toggleClass('in');
                $(sel2).toggleClass('out');
            });

  	window.appdata = JSON.parse(localStorage.getItem('eva-appdata'));
  	var pdv = 0.0;
  	var pnv = 0.0;
  	var tpd = 0.0;

  	pnv = window.appdata.pnv;
  	pdv = window.appdata.pdv;
  	tpd = pnv + pdv;

  	$('#valor-datos').text(tpd.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));
  	$('#valor-redes').text(pnv.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,'));

  	for(var i=0;i<window.appdata.pd.length;i++){
  		$('#grupos-datos').append(
  		'<div class="row" data-id="'+i+'">'+
        	'<div class="col-xs-12 cont-grupo margin-top35">'+
            '<button type="button" class="btn btn-basura pull-right glyphicon glyphicon-trash"></button>'+
            '<h1 class="app-name text-left">'+window.appdata.pd[i].name+'</h1>'+
            '<div class="col-xs-12 valor-ahora">'+
            	window.appdata.pd[i].value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')   +
            '</div>'+
          	'</div>'+
        '</div>'
  		);
  	}

    $('.btn-basura ').click(function(){
      var idx = $(this).parent().parent().data('id');
      window.appdata.pd.splice(idx,1);
      for(var i =0 ; i< window.appdata.pd.length; i++){
        window.appdata.pdv += window.appdata.pd[i].value;
      }
      localStorage.setItem('eva-appdata',JSON.stringify(window.appdata));
      document.location.reload();
    });

    $('.btn-basura2').click(function(){
        window.appdata.pnv = 0;
        localStorage.setItem('eva-appdata',JSON.stringify(window.appdata));
        document.location.reload();
    });

    $('.btn-add').click(function(){
      document.location.href="#/selecciona2";
    });

  },
  willDestroyElement: function()
  {
    this._super();

  }
});