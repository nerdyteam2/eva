import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: 'hash'
});

Router.map(function() {

	this.route('valor-grupo', { path: '/valor-grupo' });
	this.route('selecciona', { path: '/selecciona' });
	this.route('selecciona2', { path: '/selecciona2' });
	this.route('preguntas', { path: '/preguntas' });
	this.route('nivel-riesgo', { path: '/nivel-riesgo' });
	this.route('inicio', 	{ path: '/inicio' });
	this.route('home', 		{ path: '/home' });
	this.route('grupo', 	{ path: '/grupo' });
	this.route('glosario', 	{ path: '/glosario' });
	this.route('crea-grupo', { path: '/crea-grupo' });
	this.route('cerca', 	{ path: '/cerca' });

});

export default Router;
