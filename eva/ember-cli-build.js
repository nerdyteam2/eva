/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var pickFiles = require('broccoli-static-compiler');
module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    // Add options here
  });

      var css = pickFiles('vendor/css', {
           srcDir: '/',
           files: ['**/*.css'],
           destDir: '/assets/css'
       });

      var js = pickFiles('vendor/js', {
           srcDir: '/',
           files: ['**/*.js'],
           destDir: '/assets/js'
       });

      var fonts = pickFiles('vendor/fonts', {
        srcDir: '/',
        files: ['**/**'],
        destDir: '/assets/fonts'
      });

      var img = pickFiles('vendor/img', {
        srcDir: '/',
           files: ['**/*.*'],
           destDir: '/assets/img'
      });

      var bootstrap = pickFiles('vendor/bootstrap', {
        srcDir: '/',
           files: ['**/*.*'],
           destDir: '/assets/bootstrap'
      });

  return app.toTree([css,js,fonts,img,bootstrap]);
};
